# Experiment

Goal: Determine wheather 512B or 4k block size makes a difference on a Samsung 990 Pro SSD using zfs. The expected workload are various VMs.

# Simulations

Find here the various simulation descriptions. Read:Write ratios are given as e.g. 60:40, meaning that 60% of the workload i read and 40% is write.

## Simulation 1: Baseline

[Simulation 1](simulations/simulation1) acts as a generic baseline determination using 4 mixed random write and read workloads.
It simulates a 4 VM scenario where we have 3 VMs acting as mostly reading and 1 VM acting as a balanced 50:50 read/write workload on sequential data.

The 3 reading VMs have a read/write ratio of 90:10. The writing VM has a read/write ratio of 50:50. Reads and write happen sequentially.

## Simulation 2: Mixed read/write, mostly read

[Simulation 2](simulations/simulation2) simulates a 4 VM scenario with mostly read-intenstive workloads on a random mixed reads and writes pattern. It simulates 4 VMs, where 3 VMs are read-intensitve (80:20), one VM is write intensive (20:80).

## Simulation 3: Mixed read/write, mostly write

[Simulation 3](simulations/simulation3) is the same as simulation 2 but with 1 VM acting as mostly read and 3 VMs acting as mostly write.

# Setup

I use a Turing Pi RK1 (aarch64, RK3588 CPU) with 32 GiB RAM. The NVME SSD under test is the Samsung 990 Pro. Reported block size is 512B, as shown by `parted`:

```
Model: NVMe Device (nvme)
Disk /dev/nvme0n1: 2000GB
Sector size (logical/physical): 512B/512B
```

The used system for this test openSUSE Leap 15.6 with `fio-3.23` and zfs 2.2.4-1:

```
zfs-2.2.4-1
zfs-kmod-2.2.4-1
```

The used Kernel for all of those tests were `6.4.0-150600.16-default`.

Creation of the zpools happened via the following commands:

```
# zpool create ashift /dev/nvme0n1                         # Use default blocksize
# zpool create ashift12 -o ashift=12 -f /dev/nvme0n1       # 4k blocksize
```

To check the corresponding `ashift` value (used block size as a power of 2), I used

```
# zdb -C | grep ashift
ashift12:
    name: 'ashift12'
            ashift: 12
```

To create the datasets I used a simple `zfs create` command. No additional parameters have been passed:

```
zfs create ashift12/storage-test
```

`fio` is run with the provided simulation files, and the output is piped to a text file using `tee`, e.g.

```
fio simulations/simulation3 | tee results/zfs/ashift12/simulation3
```

# Resullts

The provided simulations suggest that sticking with the drive-provided 512B block size might have some non-neglibile performance advantages over a 4k block size. In both, the mostly reading and the mostly writing scenarios I observed a higher overall bandwidth and lower latency with the 512B configuration. However I also observe that the 4k configuration behaves more predictably, as the standard deviation in all scenarios with 4k as block size is considerably smaller. Also, given that the standard deviation for 512B workloads is significantly larger than for 4k, a direct comparison seems unjust. I therefore conclude that my simulations provide a qualitative argument for using 512B block sized in this configuration, but the actual numbers are to be taken with a grain of salt.

TLDR: Stick with the device provided blocksize, if performance is an objective.

See the [results.txt](results.txt) file for the extracted data table and [data.tar.gz](data.tar.gz) for the raw data files from `fio`.

## Simulation stats

Simulation 2 (mostly reading VMs) has on average 20% more throughput and latency, although also a standard deviation 20% larger. Interestingly the write throughput is 20% higher for the 4k case here so they somehow compensate each other.

In simulation 3 (mostly writing VMs) the 512B configuration outperforms the 4k by about 30% in terms of read bandwidth and 60% in write bandwidth. But also here with a 30% and 60% respectively larger standard deviation. The latency is also about 30% lower for the 512B configuration. Here 512B really shows an increased performance compared to my 4k configuration.

Looking at both the bandwidth and the runtime stats of the whole simulation gives a somehow consistent view:

* simulation1 (baseline) is somehow similar
* simulation2 (mostly reading VMs) gives `45.3/24.4MiB/s` at a runtime of `23s` for 512B vs. `38.9/20MiB/s` at a runtime of `27s`
* simulation3 (mostly writing VMs) gives `15.7/29.1MiB/s` at `36.5s` for 512B and `12.2/22.6MiB/s` at `47s`

Values are group stats and not directly translatable to nvme performance characteristics.

Given the larger standard deviation I find the overall runtime stats the more robust metric to look at. One could look at the histogram distribution that `fio` gives, but that’s beyond something I want to do now.

# Weblinks

* Post on [PracticalZFS](https://discourse.practicalzfs.com/t/block-size-alignment-with-512b-ssd/1441/7?u=phoenix)
